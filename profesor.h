#include "type.h"
#ifndef PROFESOR_H_INCLUDED
#define PROFESOR_H_INCLUDED
class profesor
{
    public:
        profesor();
        ~profesor();
        void nombres();
        void apellidos();
        void corre();
        void grado_estudio();
        void ingreso_U(I &dia,I &mes,I &anho);
        void fecha_nac(I &diaa,I &mess,I &anhoo);
        void scuela(L &codig);
    private:
        C *pr_nombre;
        C *sg_nombre;
        C *pr_apellido;
        C *sg_apellido;
        C *gradodestudio;
        C *correo;
        I dia_ingr;
        I mes_ingr;
        I a�o_ingr;
        L codi;
        I dia_naci;
        I mes_naci;
        I a�o_naci;
        C *escuela;

}


#endif // PROFESOR_H_INCLUDED
